<?php

namespace Microblau\SeoBundle\Twig;

use Twig\Environment;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Twig\Extension\AbstractExtension;

use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\Core\Helper\TranslationHelper;



class SeoExtension extends AbstractExtension
{
    private $resolver;

    private $twig;

    private $cs;

    private $translator;

    private $defaultTagsConfig;

    private $contentDefaultId;

    private $contentDefault = null;

    private $tagList;

    private $completeList = array();

    private $defaults;

    private $social_sharing;

    private $fieldsMapping;

    private $variationService;

    private $config;

    public function __construct(ConfigResolverInterface $configResolver, Environment $twig, ContentService $cs , TranslationHelper $translator, $variationService)
    {

        $this->resolver = $configResolver;
        $this->twig = $twig;
        $this->cs = $cs;
        $this->translator = $translator;
        $this->variationService = $variationService;

        // Siteaccessaware configuration load.
        $seoConfig = $this->resolver->getParameter('config', 'microblau_seo');
        if ($seoConfig) {
                $hasConfig = $configResolver->hasParameter('config', 'microblau_seo');
                if ($hasConfig) {
                    $this->config = $configResolver->getParameter('config', 'microblau_seo');
                    $this->tagList = $this->config['tag_list'];
                    $this->contentDefaultId = $this->config['contentDefaultId'];
                    if (array_key_exists('social_sharing', $this->config)) {
                        $this->social_sharing = $this->config['social_sharing'];
                    }

                    if (array_key_exists('field_mapping', $this->config)) {
                        $this->fieldsMapping = $this->config['field_mapping'];
                    }

                    if (array_key_exists('social_sharing', $seoConfig)) {
                        $this->socialSharing = $seoConfig['social_sharing'];
                    }

                    $this->gtmKey = $this->config['gtmKey'];
                    $this->gaKey = $this->config['gaKey'];
            }
        }
    }

    public function getFilters(){

        return array(
            new TwigFilter('remove_special', array($this, 'remove_special')),
        );

    }

    public function remove_special($string){
        $string = strtolower($string);
        $search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
        $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");

        return str_replace($search, $replace, $string);

    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('seo', array($this, 'seo')),
            new TwigFunction('seoValues', array($this, 'seoValues')),
            new TwigFunction('overwriteWithXrow', array($this, 'overwriteWithXrow')),
            new TwigFunction('googleTagManager', array($this, 'googleTagManager')),
            new TwigFunction('googleAnalytics', array($this, 'googleAnalytics')),
        );
    }

    public function seo($location = null, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        if ($location != null) {
            $content = $this->cs->loadContent($location->contentInfo->id);
            $this->getFieldValues($this->fieldsMapping, $content);

            return $this->twig->display('MicroblauSeoBundle::seo.html.twig', array( 'tagList' => $this->completeList,
                                                                                    'gtmKey' => $this->gtmKey ));

        }

        return null;
    }

    public function seoValues($location = null, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        if ($location != null) {

            $content = $this->cs->loadContent($location->contentInfo->id);
            $this->getFieldValues($this->fieldsMapping, $content);
            return $this->completeList;

        }

        return null;
    }

    public function overwriteWithXrow($seoValues = null, $xrowFieldValues = null)
    {
        foreach($xrowFieldValues as $key => $value){
            if( $value != '' ){
                $seoValues[$key] = $value;
            }
        }

        return $this->twig->display('MicroblauSeoBundle::seo.html.twig', array('tagList' => $seoValues));
    }

    public function googleTagManager()
    {
        return $this->twig->display('MicroblauSeoBundle::gtm.html.twig', array('gtmKey' => $this->gtmKey));
    }

    public function googleAnalytics()
    {
        return $this->twig->display('MicroblauSeoBundle::ga.html.twig', array('gaKey' => $this->gaKey));
    }

    public function getFieldValues($mapping, $content): void
    {
        $contentTypeId = $content->contentInfo->contentTypeId;
        if ( isset($mapping['class_specific_fields']) &&
                array_key_exists($contentTypeId, $mapping['class_specific_fields']) )
        {
            foreach ($mapping['class_specific_fields'][$contentTypeId] as $tagId => $mappedField)
            {
                if (is_null($content->getFieldValue($mappedField)))
                {
                    continue;
                }

                if ($tagId === 'og_image')
                {
                    $this->completeList[$tagId] = $this->getImageFieldUri( $this->translator->getTranslatedField($content, $mappedField) , $content);
                }
                else
                {
                    $this->completeList[$tagId] = $this->translator->getTranslatedField($content, $mappedField)->value;
                }
            }
        }

        foreach ($this->tagList as $tagId => $value)
        {
            if (!array_key_exists($tagId, $this->completeList) || !trim($this->completeList[$tagId]))
            {
                $this->getDefaultTag($content, $tagId, $value);
            }
        }
    }

    public function getDefaultTag($content, $tagId, $value): void
    {
        if (is_null($this->contentDefault))
        {
            $this->contentDefault = $this->cs->loadContent($this->contentDefaultId);
        }

        $isImageField = false;
        // Check, if tagId is a mapped field
        if ( isset($this->fieldsMapping['defaults'][$tagId]) )
        {
            $defaultFieldMapped = $this->fieldsMapping['defaults'][$tagId];

            if ($content->getFieldValue($defaultFieldMapped) && !isset($content->getFieldValue($defaultFieldMapped)->xml))
            {
                if($tagId === 'og_image')
                {
                    $this->completeList[$tagId] = $this->getImageFieldUri($content->getField($defaultFieldMapped), $content);
                    $isImageField = true;
                }
                else
                {
                    $this->completeList[$tagId] = $this->translator->getTranslatedField($content, $defaultFieldMapped)->value;
                }
            }
            else if ($this->contentDefault->getFieldValue($defaultFieldMapped) && !isset($this->contentDefault->getFieldValue($defaultFieldMapped)->xml))
            {
                if($this->isImageField($this->contentDefault->getFieldValue($defaultFieldMapped)))
                {
                    $this->completeList[$tagId] = $this->getImageFieldUri($this->contentDefault->getField($defaultFieldMapped), $content);
                    $isImageField = true;
                }
                else
                {
                    $this->completeList[$tagId] = $this->translator->getTranslatedField( $this->contentDefault, $defaultFieldMapped)->value;
                }
            }
        }

        // Set fallback value from default list
        if (!isset($this->completeList[$tagId]) || ( !trim($this->completeList[$tagId]) && trim($value) ))
        {
            $this->completeList[$tagId] = $value;
            if ($isImageField)
            {
                $this->setImageSizeFromURI($value);
            }
        }
    }

    public function isImageField($field)
    {
        if (get_class($field) == 'eZ\Publish\Core\FieldType\Image\Value')
        {
            return true;
        }

        return false;
    }

    public function getImageFieldUri($field, $content)
    {
        try
        {
            $imageVariationService = $this->variationService;
            $imageAlias = $this->getImageAliasIdentifier();
            $imageVariation = $imageVariationService->getVariation($field, $content->versionInfo, $imageAlias);
            if ($imageVariation != null)
            {
                $this->setImageSizeFromURI($imageVariation->uri);
                return $imageVariation->uri;
            }
        }
        catch (\Exception $exception)
        {
        }

        return false;
    }

    /**
     *
     * @return string
     */
    public function getImageAliasIdentifier()
    {
        if (isset($this->config['og_image_variation']))
        {
            return $this->config['og_image_variation'];
        }
        else
        {
            return "original";
        }
    }

    /**
     *
     */
    public function setImageSizeFromURI( $uri ): void
    {
        try
        {
            $imageSize = getimagesize($uri);
            $this->completeList['og_image_width'] = $imageSize[0];
            $this->completeList['og_image_height'] = $imageSize[1];
        }
        catch (\Exception $exception)
        { }

        return;
    }

    public function getName()
    {
        return 'seo_extension';
    }
}
