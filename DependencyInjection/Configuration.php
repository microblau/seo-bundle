<?php

namespace Microblau\SeoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\SiteAccessAware\Configuration as SiteAccessConfiguration;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration extends SiteAccessConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('microblau_seo');
        $rootNode = $treeBuilder->getRootNode();

        // $systemNode will then be the root of siteaccess aware settings.
        $systemNode = $this->generateScopeBaseNode( $rootNode );
        $systemNode
            ->arrayNode( 'config' )
                ->children()
                    ->arrayNode( 'tag_list' )
                        ->prototype('scalar')->end()
                    ->end()
                ->arrayNode( "social_sharing" )
                    ->prototype('scalar')->end()
                ->end()
                ->scalarNode("contentDefaultId")->end()
                ->scalarNode("gtmKey")->end()
                ->scalarNode("gaKey")->end()
                ->scalarNode("og_image_variation")->end()
                ->arrayNode( 'field_mapping' )
                    ->children()
                        ->arrayNode( 'defaults' )
                            ->children()
                                ->scalarNode( "title" )->end()
                                ->scalarNode( "keywords" )->end()
                                ->scalarNode( "description" )->end()
                                ->scalarNode( "og_image" )->end()
                                ->end()
                        ->end()
                        ->arrayNode( 'class_specific_fields' )
                            ->prototype('array')
                                ->prototype('scalar')->end()
                            ->end()

                        ->end()
                ->end()
            ->end();

        return $treeBuilder;

    }
}
