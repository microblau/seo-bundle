<?php

namespace Microblau\SeoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends AbstractController
{
    public function indexAction($name)
    {
        return $this->render('MicroblauSeoBundle:Default:index.html.twig', array('name' => $name));
    }
}
