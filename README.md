## Bundle basic configuration

    microblau_seo:
        system:
            siteaccess_group_a:
                config:
                    tag_list:
                        title:                "Default Title"
                        viewport:             "viewport config"
                        keywords:             "Default keywords"
                        description:          "Default description"
                    social_sharing:       ['google']
                    contentDefaultId:     3215
                    og_image_variation:    "original"
                    field_mapping:
                        defaults:
                            title:                title
                            keywords:             meta_keywords
                            description:          meta_description
                        class_specific_fields:
                            74:
                                og_image: featured_bg_image

            siteaccess_group_b:
                config:
                    tag_list:
                        title:                "Default Title reemrpresa"
                        viewport:             "viewport config reemrpresa"
                        keywords:             "Default keywords reemrpresa"
                        description:          "Default description reemrpresa"
                        og_image: "http://reempresa.dev/var/ezdemo_site/storage/images/cecot-home/actualitat/marc-laboral/actualitat-2/30650-9-cat-ES/ACtualitat-2.png"
                    social_sharing:       ['google']
                    contentDefaultId:     57
                    field_mapping:
                        defaults:
                            title:                title
                            keywords:             meta_keywords
                            description:          meta_description
                        class_specific_fields: []


----------
## Todos

 - Image variations. create specific vatiation on install? og width and heigh?
 - Ensure individual siteaccess are overriding groups

